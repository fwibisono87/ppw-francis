from django.shortcuts import render
from datetime import datetime, date
import csv


desc_file = "lab_1/static/description.txt"
with open(desc_file) as desc:
    desc_text = desc.readlines()

list_edu = []
edu_file ="lab_1/static/education.csv"
with open(edu_file) as edu:
    reader = csv.reader(edu)
    for row in reader:
        list_edu.append(row)


hobbies_file="lab_1/static/hobbies.txt"
with open(hobbies_file) as hob:
    hobbies = hob.readlines()


mhs_name = 'Francis Wibisono'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2001, 8, 24)
npm = 1906350553
organization = "Fakultas Ilmu Komputer, Universitas Indonesia"
url_foto = "https://maung.id/static/3d987f954f136623627cd3e5b7b50973/9f583/FR25.jpg"

linkedin = "https://www.linkedin.com/in/francis-wibisono"
instagram = "https://www.instagram.com/fwibisono87"



def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'org': organization,\
               'hobbies': hobbies, 'foto': url_foto, 'desc': desc_text, 'edu': list_edu, 'linkedin': linkedin,\
                'instagram':instagram}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

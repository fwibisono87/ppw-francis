from django.urls import path
from . import views

# url for app, add your URL Configuration

urlpatterns = [
    path('', views.index)
]
